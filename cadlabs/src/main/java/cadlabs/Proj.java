package cadlabs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.stream.Collectors;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.mllib.linalg.SparseVector;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.linalg.distributed.CoordinateMatrix;
import org.apache.spark.mllib.linalg.distributed.MatrixEntry;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;

import scala.Tuple2;

public class Proj {

	private static final String DefaulftFile = "data/flights.small.csv";

	private static int node;
	private static SparkSession spark;

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);

		String file = (args.length < 1) ? DefaulftFile : args[0];

		// start Spark session (SparkContext API may also be used)
		// master("local") indicates local execution
		spark = SparkSession.builder().appName("FlightAnalyser").master("local[*]").getOrCreate();

		// only error messages are logged from this point onward
		// comment (or change configuration) if you want the entire log
		spark.sparkContext().setLogLevel("ERROR");

		Dataset<String> textFile = spark.read().textFile(file).as(Encoders.STRING());

		/*
		 * Dataset with all flight info
		 */
		Dataset<Row> flights = textFile.map((MapFunction<String, Row>) l -> Flight.parseFlight(l), Flight.encoder())
				.cache();
		/*
		 * Build an undirected graph to represent the average delay of the flights
		 * between any two airports (the average must consider all flights between the
		 * both airports, disregarding the origin and the destination). The graph�s
		 * nodes are thus the airports, and the edges represent direct routes between
		 * airports, labelled with the average delays of the routes� flights.
		 */
		Dataset<Row> flightDistance = flights.select("origInternalId", "destInternalId", "departure delay")
				.union(flights.select(flights.col("destInternalId").as("origInternalId"),
						flights.col("origInternalId").as("destInternalId"), flights.col("departure delay")))
				.groupBy("origInternalId", "destInternalId").agg(functions.avg("departure delay"));

		int numberAirports = (int) flights.groupBy("origInternalId").count().count();

		/*
		 * Creating the adjacent matrix
		 */
		JavaRDD<MatrixEntry> entries = flightDistance.toJavaRDD().map(f -> new MatrixEntry(f.getLong(0), f.getLong(1),
				f.getDouble(2) == 0 ? f.getDouble(2) + 0.01 : f.getDouble(2)));

		JavaPairRDD<Long, Vector> matrix = new CoordinateMatrix(entries.rdd()).transpose().toIndexedRowMatrix().rows()
				.toJavaRDD().mapToPair(r -> new Tuple2<Long, Vector>(r.index(), r.vector()));

		System.out.println("Initial matrix");
		for (Tuple2<Long, Vector> lv : matrix.collect()) {
			System.out.println(lv);
		}
		System.out.println();

		/*
		 * Creating the d vector
		 */
		List<Tuple2<Long, Tuple2<Double, Tuple2<Boolean, Long>>>> l = new ArrayList<Tuple2<Long, Tuple2<Double, Tuple2<Boolean, Long>>>>(
				numberAirports);
		for (int i = 0; i < numberAirports; i++)
			l.add(i, new Tuple2<Long, Tuple2<Double, Tuple2<Boolean, Long>>>((long) i,
					new Tuple2<Double, Tuple2<Boolean, Long>>(Double.MAX_VALUE,
							new Tuple2<Boolean, Long>(false, Long.MAX_VALUE))));

		@SuppressWarnings("resource")
		JavaPairRDD<Long, Tuple2<Double, Tuple2<Boolean, Long>>> d = new JavaSparkContext(spark.sparkContext())
				.parallelize(l).mapToPair(r -> r);

		System.out.println("Number of airports " + numberAirports);
		
		/*
		 * Compute the graph�s Minimum Spanning Tree (MST) by implementing the parallel
		 * version of Prim�s Algorithm (available from CLIP). The MST will be the
		 * subgraph of the original with the minimum total edge weight (sum of the
		 * average delays). Output the MST and its total edge weight.
		 */
		Random rand = new Random();
		d = compMst(numberAirports, matrix, d, rand);

		/*
		 * Identify the bottleneck airport, i.e. the airport with higher aggregated
		 * delay time (sum of the delays of all routes going out of the airport) from
		 * the ones contained in the complement graph of the MST previously computed.
		 */
		long bottleneck = calcBottleneck(flights, numberAirports, matrix, d);

		/*
		 * Modify the graph to reduce by a given factor the delay time of all routes
		 * going out of the selected airport. This factor will be a parameter of your
		 * algorithm (received in the command line) and must be a value in ]0, 1[
		 */
		matrix = applyFactor(sc, numberAirports, matrix, bottleneck);
		
		/*
		 * Recompute the MST and display the changes perceived in the resulting subgraph and on the sum of the total edge weight.
		 */
		d = compMst(numberAirports, matrix, d, rand);
		
		// terminate the session
		sc.close();
		spark.stop();
	}

	private static JavaPairRDD<Long, Vector> applyFactor(Scanner sc, int numberAirports,
			JavaPairRDD<Long, Vector> matrix, long bottleneck) {
		System.out.println("Apply factor(Double from 0 to 1 exclusively):");
		double factor = sc.nextDouble();
		while(factor<=0||factor>=1) {
			System.out.println("Must be value from 0 to 1:");
			factor=sc.nextDouble();
		}

		matrix = factorMultiply(bottleneck, factor, numberAirports, matrix);
		System.out.println();
		System.out.println("Matrix after factor");
		for (Tuple2<Long, Vector> lv : matrix.collect()) {
			System.out.println(lv);
		}
		System.out.println();
		return matrix;
	}

	private static long calcBottleneck(Dataset<Row> flights, int numberAirports, JavaPairRDD<Long, Vector> matrix,
			JavaPairRDD<Long, Tuple2<Double, Tuple2<Boolean, Long>>> d) {
		JavaRDD<MatrixEntry> mstE = d.map(f -> new MatrixEntry(f._1, f._2._2._2, f._2._1));
		JavaRDD<MatrixEntry> mstO = d.map(f -> new MatrixEntry(f._2._2._2, f._1, f._2._1));

		JavaPairRDD<Long, Vector> mstEP = new CoordinateMatrix(mstE.rdd()).transpose().toIndexedRowMatrix().rows()
				.toJavaRDD().mapToPair(r -> new Tuple2<Long, Vector>(r.index(), r.vector()));
		JavaPairRDD<Long, Vector> mstOP = new CoordinateMatrix(mstO.rdd()).transpose().toIndexedRowMatrix().rows()
				.toJavaRDD().mapToPair(r -> new Tuple2<Long, Vector>(r.index(), r.vector()));

		JavaPairRDD<Long, Vector> mst = mstEP.union(mstOP).reduceByKey((x, y) -> mstCombine(x, y));


		// create sum array
		List<Tuple2<Long, Double>> li = new ArrayList<Tuple2<Long, Double>>(numberAirports);
		for (int i = 0; i < numberAirports; i++)
			li.add(i, new Tuple2<Long, Double>((long) i, 0.0));

		@SuppressWarnings("resource")
		JavaPairRDD<Long, Double> di = new JavaSparkContext(spark.sparkContext()).parallelize(li).mapToPair(r -> r);

		JavaPairRDD<Long, Tuple2<Vector, Vector>> graphWithoutMst = mst.join(matrix);

		JavaPairRDD<Long, Tuple2<Double, Tuple2<Vector, Vector>>> dimatrix = di.join(graphWithoutMst);

		di = dimatrix.mapToPair(e -> new Tuple2<Long, Double>(e._1, sumOfNodes(e)));

		long bottleneck = di.reduce((x, y) -> x._2 > y._2 ? x : y)._1;
		String c = "origInternalId=" + bottleneck;
		System.out.println();
		System.out.println("Bottleneck airport id: " + flights.where(c).select("org_id", "origin").first());
		System.out.println();
		return bottleneck;
	}

	private static JavaPairRDD<Long, Tuple2<Double, Tuple2<Boolean, Long>>> compMst(int numberAirports,
			JavaPairRDD<Long, Vector> matrix, JavaPairRDD<Long, Tuple2<Double, Tuple2<Boolean, Long>>> d, Random rand) {
		node = rand.nextInt(numberAirports);
		// node = 2;
		System.out.println("Computing MST:");
		for (int i = 0; i < numberAirports; i++) {

			// global variable gives problems because it is an "object"
			int n = new Integer(node);

			System.out.println("------------------- iteration " + i + ": process node " + n);
			JavaPairRDD<Long, Tuple2<Tuple2<Double, Tuple2<Boolean, Long>>, Vector>> dmatrix = d.join(matrix);

			d = dmatrix.mapToPair(e -> new Tuple2<Long, Tuple2<Double, Tuple2<Boolean, Long>>>(e._1, updateD(n, e._2)));

			d = d.mapToPair(
					e -> e._1 == n
							? new Tuple2<Long, Tuple2<Double, Tuple2<Boolean, Long>>>(e._1,
									new Tuple2<Double, Tuple2<Boolean, Long>>(e._2._1,
											new Tuple2<Boolean, Long>(true, e._2._2._2)))
							: e);

			node = d.reduce((x, y) -> compareNodes(x, y))._1.intValue();
			System.out.println("node:" + node);
			for (Tuple2<Long, Tuple2<Double, Tuple2<Boolean, Long>>> e : d.collect()) {
				System.out.println(e);
			}

		}

		System.out.println();
		System.out.println("Matrix representation of Minimum Spanning Tree:");

		for (Tuple2<Long, Tuple2<Double, Tuple2<Boolean, Long>>> e : d.collect()) {
			int t = e._2._2._2.intValue();

			for (int i = 0; i < numberAirports; i++) {
				if (i == t) {
					Double tmp = e._2._1;
					System.out.print((tmp <= 0.01 ? 0 : tmp) + " ");
				} else {
					System.out.print("[] ");
				}
			}
			System.out.println();
		}
		;
		return d;
	}

	private static Double calcSum(Double x, Double y) {
		if (x < Double.MAX_VALUE && x > 0.01 && y < Double.MAX_VALUE && y > 0.01)
			return x + y;
		else if (y < Double.MAX_VALUE && y > 0.01) {
			return y;
		} else if (x < Double.MAX_VALUE && x > 0.01) {
			return x;
		}
		return 0.0;
	}

	private static Tuple2<Double, Tuple2<Boolean, Long>> updateD(int node,
			Tuple2<Tuple2<Double, Tuple2<Boolean, Long>>, Vector> entry) {

		SparseVector sv = entry._2.toSparse();
		double value = sv.apply(node);
		double e = entry._1._1;

		if (value == 0) {
			return entry._1;
		} else if (value < e)
			return new Tuple2<Double, Tuple2<Boolean, Long>>(value,
					new Tuple2<Boolean, Long>(entry._1._2._1, (long) node));
		else
			return entry._1;
	}

	private static Tuple2<Long, Tuple2<Double, Tuple2<Boolean, Long>>> compareNodes(
			Tuple2<Long, Tuple2<Double, Tuple2<Boolean, Long>>> n1,
			Tuple2<Long, Tuple2<Double, Tuple2<Boolean, Long>>> n2) {
		boolean firstTupleVisited = n1._2._2._1;
		boolean sndTupleVisited = n2._2._2._1;
		double n1Weight = n1._2._1;
		double n2Weight = n2._2._1;

		if (firstTupleVisited == false && sndTupleVisited == false) {
			if (n1Weight < n2Weight) {

				return n1;
			} else
				return n2;
		} else if (firstTupleVisited == true && sndTupleVisited == false) {
			return n2;
		} else if (firstTupleVisited == false && sndTupleVisited == true) {
			return n1;
		} else {
			return n1;
		}
	}

	private static Vector mstCombine(Vector t1, Vector t2) {
		if (t1 == null) {
			return t2;
		} else if (t2 == null) {
			return t1;
		} else {
			SparseVector sv1 = t1.toSparse();
			SparseVector sv2 = t2.toSparse();
			int[] sv1Ind = sv1.indices();
			int[] sv2Ind = sv2.indices();
			List<Integer> ind = new ArrayList<Integer>();
			List<Double> values = new ArrayList<Double>();
			for (int i = 0; i < sv1Ind.length; i++) {
				ind.add(sv1Ind[i]);
				values.add(sv1.values()[i]);
			}
			for (int i = 0; i < sv2Ind.length; i++) {
				if (!ind.contains(sv2Ind[i])) {
					ind.add(sv2Ind[i]);
					values.add(sv2.values()[i]);
				}
			}

			int[] indices = new int[ind.size()];
			double[] value = new double[ind.size()];

			for (int l = 0; l < ind.size(); l++) {
				indices[l] = ind.get(l);
				value[l] = values.get(l);
			}
			return Vectors.sparse(4, indices, value);
		}
	}

	// parametro tuple2<vector,vector> - vector da esquerda, grafo completo, vector
	// da direita, mst
	// soma das arestas de cada node no vector da esquerda e subtra
	@SuppressWarnings("resource")
	private static Double sumOfNodes(Tuple2<Long, Tuple2<Double, Tuple2<Vector, Vector>>> e) {
		SparseVector sv1 = e._2._2._1.toSparse();
		SparseVector sv2 = e._2._2._2.toSparse();

		List<Double> di1 = new ArrayList<Double>();
		for (double l : sv1.values()) {
			di1.add(l);
		}
		Double sum1 = 0.0;
		if (di1.size() > 1) {
			JavaRDD<Double> k1 = new JavaSparkContext(spark.sparkContext()).parallelize(di1).map(r -> r);
			sum1 = k1.reduce((x, y) -> calcSum(x, y));
		} else {
			sum1 = di1.get(0);
		}

		List<Double> di2 = new ArrayList<Double>();
		for (double l : sv2.values()) {
			di2.add(l);
		}

		Double sum2 = 0.0;
		if (di2.size() > 1) {
			JavaRDD<Double> k2 = new JavaSparkContext(spark.sparkContext()).parallelize(di2).map(r -> r);
			sum2 = k2.reduce((x, y) -> calcSum(x, y));
		} else {
			sum2 = di2.get(0);
		}
		return sum1 > 0.01 && sum2 > 0.01 ? Math.abs(sum1 - sum2) : sum1 > 0.01 ? sum1 : sum2 > 0.01 ? sum2 : 0.0;
	}

	private static JavaPairRDD<Long, Vector> factorMultiply(long aeroport, double factor, int numberAirports,
			JavaPairRDD<Long, Vector> matrix) {

		return matrix.mapToPair(e -> multiply(e, aeroport, numberAirports, factor));

	}

	private static Tuple2<Long, Vector> multiply(Tuple2<Long, Vector> e, long aeroport, int numberAirports,
			double factor) {
		if (e._1 == aeroport) {
			SparseVector sv = e._2.toSparse();
			return new Tuple2<Long, Vector>(e._1,
					Vectors.sparse(numberAirports, sv.indices(), arrayMult(sv.values(), factor)));
		} else {
			return collumnMultiply(e, aeroport, factor);
		}
	}

	private static double[] arrayMult(double[] array, double factor) {
		for (int i = 0; i < array.length; i++)
			array[i] *= factor;
		return array;
	}

	private static Tuple2<Long, Vector> collumnMultiply(Tuple2<Long, Vector> e, long aeroport, double factor) {
		SparseVector sv = e._2.toSparse();
		List<Integer> list = Arrays.stream(sv.indices()).boxed().collect(Collectors.toList());
		if (list.indexOf((int) aeroport) != -1) {
			sv.values()[list.indexOf((int) aeroport)] *= factor;
		}

		return new Tuple2<Long, Vector>(e._1, Vectors.sparse(4, sv.indices(), sv.values()));
	}

}
